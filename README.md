# Spigot Archetype

**This project is a simple [Maven Archetype](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html) for Spigot Plugins**

## Features

- Creates `pom.xml` with Spigot API as dependency.  
- Enables Maven Incremental Budiling.  
- Sets the Java version to `1.8`.  
- Creates `plugin.yml` file with placeholders to the project info.  
- Replaces the placeholders in the `plugin.yml` file while building.  
- Creates the `Main.java` class file.  

## How to use

You can checkout the [Maven Official Documentation](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html) or just follow the steps bellow.

### Cloning the repository

> $ `git clone https://gitlab.com/Pauloo27/SpigotArchetype.git`  
> $ `cd SpigotArchetype` 

### Building the project

> $ `mvn install`

### Updating the Maven Archetypes catalog

> $ `mvn install archetype:update-local-catalog`

### Creating a new project using the Archetype
**Open your projects folder and run:**

> $ `mvn archetype:generate -DarchetypeCatalog=local`

**You'll have to select the right Archetype and then fill the repository artifact and group.
The project folder will be named with the artifact name.**

## License

[![GPL3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.html)  
See the [LICENSE](./LICENSE) file.

